package com.example.win_pc.cmscanner;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.example.win_pc.cmscanner.R.layout.activity_main;

public class MainActivity  extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
//Button b5;
ImageView im;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(activity_main);
//b5=(Button)findViewById(R.id.gen);
im=(ImageView)findViewById(R.id.al);


        im.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                Intent login = new Intent(getApplicationContext(), album.class);

                startActivity(login);

                finish();
            }
        });
        mScannerView = (ZXingScannerView) findViewById(R.id.view_scanner);}
    final MediaPlayer[] mp1 = new MediaPlayer[1];



    @Override
    public void handleResult(Result rawResult) {
        Log.v("TAG", rawResult.getText()); // Prints scan results
        // Prints the scan format (qrcode, pdf417 etc.)
        Log.v("TAG", rawResult.getBarcodeFormat().toString());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Scan Result");
        builder.setMessage(rawResult.getText());
        AlertDialog alert1 = builder.create();
        alert1.show();
//
//            String s = "http://www.google.com/search?q=";
//        s +=rawResult.getText() ;

        switch (rawResult.getText()) {
            case "Hopscotch.mp3": {


                Intent intent = new Intent(getApplicationContext(), music.class);
                intent.putExtra("name", rawResult.getText());
                startActivity(intent);




//                Intent intent = new Intent();
//
//                intent.setAction(Intent.ACTION_VIEW);
//                ComponentName comp = new ComponentName("com.android.music",
//                        "com.android.music.MediaPlaybackActivity");
//                intent.setComponent(comp);


//            //    String videoUri = "android.resource://com.example.win_pc.cmscanner/"+R.raw.hopscotch;
//               // InputStream in = getResources().openRawResource(R.raw.hopscotch)


             //   RingtoneManager.getRingtone(this,Uri.parse("android.resource://com.example.win_pc.cmscanner/" + R.raw.hopscotch)).play();



//                    intent.setDataAndType( Uri.parse("android.resource://" + getPackageName() + "/" + getAssets("Hopscotch.mp3"))), "audio");
////
////
//
//                startActivity(intent);

                break;
            }
            case "Colors.mp3": {
                Intent intent = new Intent(getApplicationContext(), music.class);
                intent.putExtra("name", rawResult.getText());
                startActivity(intent);
//                Intent intent = new Intent();
//
//                intent.setAction(Intent.ACTION_VIEW);
//
//                File audioFile = new File("storage/emulated/0", "Music/Audio/Colors.mp3");
//                intent.setDataAndType(Uri.fromFile(audioFile), "audio");
//                intent.setPackage("com.google.android.music");
//                startActivity(intent);
//                mp1[0] = MediaPlayer.create(this, R.raw.colors);
//                mp1[0].setLooping(true);
//                mp1[0].start();
                break;
            }
            case "Hidden_in_Your_Heart.mp3": {
                Intent intent = new Intent(getApplicationContext(), music.class);
                intent.putExtra("name", rawResult.getText());
                startActivity(intent);
//                Intent intent = new Intent();
//
//                intent.setAction(Intent.ACTION_VIEW);
//
//                File audioFile = new File("storage/emulated/0", "Music/Audio/Hidden_in_Your_Heart.mp3");
//                intent.setDataAndType(Uri.fromFile(audioFile), "audio");
//                intent.setPackage("com.google.android.music");
//                startActivity(intent);
//                mp1[0] = MediaPlayer.create(this, R.raw.hidden);
//                mp1[0].setLooping(true);
//                mp1[0].start();
                break;

            }
            case "Lincoln.mp3": {
                Intent intent = new Intent(getApplicationContext(), music.class);
                intent.putExtra("name", rawResult.getText());
                startActivity(intent);
//                Intent intent = new Intent();
//
//                intent.setAction(Intent.ACTION_VIEW);
//
//                File audioFile = new File("storage/emulated/0", "Music/Audio/Lincoln.mp3");
//                intent.setDataAndType(Uri.fromFile(audioFile), "audio");
//                intent.setPackage("com.google.android.music");
//                startActivity(intent);
//                mp1[0] = MediaPlayer.create(this, R.raw.lincoln);
//                mp1[0].setLooping(true);
//                mp1[0].start();
              break;

            }
            case "Learn.m4v": {
                Intent intent = new Intent(getApplicationContext(), result.class);
                intent.putExtra("name", rawResult.getText());
                startActivity(intent);

                break;
            }
            case "The_Life_&_Times_of_a_Contortionist.m4v": {
//                Intent intent = new Intent();
//
//                intent.setAction(Intent.ACTION_VIEW);
//
//                File audioFile = new File("storage/emulated/0", "Video/The_Life_&_Times_of_a_Contortionist.m4v");
//                intent.setDataAndType(Uri.fromFile(audioFile), "video");
//
//                startActivity(intent);
                Intent intent = new Intent(getApplicationContext(), result.class);
                intent.putExtra("name", rawResult.getText());
                startActivity(intent);

                break;
            }

            default:
                Toast.makeText(this, " Wrong BarCode Please Try Again",
                        Toast.LENGTH_LONG).show();
                break;
        }
        mScannerView.resumeCameraPreview(this);

        // If you would like to resume scanning, call this method below:



    }


    public void onResume() {
        super.onResume();
        // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);
        // Start camera on resume
        mScannerView.startCamera();

    }



    @Override
    public void onPause() {
        super.onPause();
        // Stop camera on pause
        mScannerView.stopCamera();

    }
    public void onBackPressed(){
        mp1[0].stop();
        mp1[0].release();
    }

//    private void copyasset()
//    {
//        AssetManager assetManager = getAssets();
//
//        InputStream in = null;
//        OutputStream out = null;
//        File file = new File(getFilesDir(), "Hopscotch.mp3");
//        try
//        {
//            in = assetManager.open("Hopscotch.mp3");
//            out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);
//
//            copyFile(in, out);
//            in.close();
//            in = null;
//            out.flush();
//            out.close();
//            out = null;
//        } catch (Exception e)
//        {
//            Log.e("tag", e.getMessage());
//        }
//
////        Intent intent = new Intent(Intent.ACTION_VIEW);
////        intent.setDataAndType(
////                Uri.parse("file://" + getFilesDir() + "/Hopscotch.mp3"),
////                "audio");
//        MediaPlayer mp = MediaPlayer.create(this, Uri.fromFile(file));
//       mp.start();
//    }
//
//    private void copyFile(InputStream in, OutputStream out) throws IOException
//    {
//        byte[] buffer = new byte[1024];
//        int read;
//        while ((read = in.read(buffer)) != -1)
//        {
//            out.write(buffer, 0, read);
//        }
//    }


}
//    public void onActivityResult(int requestCode,  resultCode, Intent intent) {
//
//        super.onActivityResult(requestCode, resultCode, intent);
//
//        if (requestCode == 0) {
//            if (resultCode == RESULT_OK) {
//                String contents = intent.getStringExtra("SCAN_RESULT");
//                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
//
//                // Handle successful scan
//
//                String s = "http://www.google.com/search?q=";
//                s += contents;
//                Intent myIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(s));
//                startActivity(myIntent1);
//            } else if (resultCode == RESULT_CANCELED) {
//                // Handle cancel
//            }
//        }}

