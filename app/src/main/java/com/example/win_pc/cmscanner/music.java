package com.example.win_pc.cmscanner;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.VideoView;

public class music extends AppCompatActivity  {
    SeekBar seek;
    ImageButton play;
    VideoView vu;
    int len = 0;
    boolean isPlaying = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);


        String r = getIntent().getStringExtra("name");

        switch (r) {
            case "Hopscotch.mp3": {

                VideoView view = (VideoView)findViewById(R.id.vid2);
                String path = "android.resource://" + getPackageName() + "/" + R.raw.hopscotch;
                view.setVideoURI(Uri.parse(path));
                view.start();
                MediaController mediaController = new MediaController(this);
// initiate a video view

// set media controller object for a video view
                view.setMediaController(mediaController);


                break;
            }
            case "Colors.mp3": {
                VideoView view = (VideoView)findViewById(R.id.vid2);
                String path = "android.resource://" + getPackageName() + "/" + R.raw.colors;
                view.setVideoURI(Uri.parse(path));
                view.start();
                MediaController mediaController = new MediaController(this);
                view.setMediaController(mediaController);
                break;

            }
            case "Hidden_in_Your_Heart.mp3": {
                VideoView view = (VideoView)findViewById(R.id.vid2);
                String path = "android.resource://" + getPackageName() + "/" + R.raw.hidden;
                view.setVideoURI(Uri.parse(path));
                view.start();
                MediaController mediaController = new MediaController(this);
                view.setMediaController(mediaController);
                break;

            }
            case "Lincoln.mp3": {
                VideoView view = (VideoView)findViewById(R.id.vid2);
                String path = "android.resource://" + getPackageName() + "/" + R.raw.lincoln;
                view.setVideoURI(Uri.parse(path));
                view.start();
                MediaController mediaController = new MediaController(this);
                view.setMediaController(mediaController);
                break;

            }



        }

    }


    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        finish();
    }
}
